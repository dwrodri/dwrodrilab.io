The following is a selection of projects I've worked on over the past few years
to give you an idea of my interests as an ML engineer and what sort of work I'm
passionate about. At the moment, I'm in search of full-time work as a
mid-career ML engineer where I would help build data-driven software alongside
other talented engineers. At the moment, I'm looking for full-time roles outside of
 the defense industry.

## Topic Search (2022)
*Technologies: [Python](https://www.python.org/), [SpaCy](https://spacy.io),
[Pandas](https://pandas.pydata.org/), [Apache
Kafka](https://kafka.apache.org/), [Protobuf](https://protobuf.dev/), [Docker
Compose](https://docs.docker.com/compose/)*
<br>
<br>
I was contracted by a stealth-mode social media startup to build out a topic
modeling service that would power a rewards system which would help monitor and
encourage discussion related to current events, food, helpful advice, and humor.

I curated a labelled dataset out of 6 years of reddit data and then used to
[SpaCy](https://spacy.io) to build a supervised topic classification pipeline
that could be deployed in their production environment.

To test my codebase, I replicated all of the infrastructure they were using in
production on my own server. This involved a mixture of setting up [Confluent's
Schema Registry](https://docs.confluent.io/home/overview.html), [Apache
Kafka](https://kafka.apache.org/), as well as using [GitLab's
CI/CD](https://docs.gitlab.com/ee/ci/) for automated generation of the
[protobuf](https://protobuf.dev/) Python packages hosted in [GitLab's Package
Registry](https://docs.gitlab.com/ee/user/packages/package_registry/) for
passing data between my code and the rest of the platform.

In the end I achieved an a **validation AUC ROC of >.93 across all topics**, and was
able to run inference more than **44,000+ median-sized posts per second on the
[e2-micro instance provided by
GCP](https://cloud.google.com/compute/vm-instance-pricing#e2_sharedcore_machine_types),
effectively keeping the entire service within the free quota provided to a
standard account on Google Cloud Platform.**

##  MotionX (2020-2022)
*Technologies: [C++17](https://isocpp.org/), [OpenCV](https://opencv.org/),
[FFmpeg](https://ffmpeg.org/),
[OpenVINO](https://www.intel.com/content/www/us/en/developer/tools/openvino-toolkit/overview.html),
[DeepStream SDK](https://developer.nvidia.com/deepstream-sdk)*
<br>
<br>
While working as a contractor at
[Cloudastructure](https://www.cloudastructure.com/), I was tasked with leading
the rewrite of the code they deployed at the edge. I designed a computer vision
pipeline that could do motion sensing similar to the functionality provided by
the [Motion project](https://motion-project.github.io/). This was eventually
extended to include a low-latency notification system that could send SMS and
Email notifications when people where detected on the camera feed. All of this
was deployed on small form factor (SFF) PCs that did not have dedicated GPUs.

I also lead the research into transitioning our edge platform to the [Nvidia
Jetson](https://www.nvidia.com/en-us/autonomous-machines/embedded-systems/)
platform, and went so far as to demonstrate a proof-of-concept powered by the
[DeepStream SDK](https://developer.nvidia.com/deepstream-sdk)

## Koi No Yokan (2021)

*Technologies: [Python](https://www.python.org/), [NumPy](https://numpy.org/),
[Pandas](https://pandas.pydata.org/), [FastAPI](https://fastapi.tiangolo.com/)*
<br>
<br>
Koi No Yokan is a search engine for anime. I've collected a a corpus of reviews
compirising of 43,020 different shows and movies, and made it possible to search
for shows using a simple [TF-IDF
search](https://nlp.stanford.edu/IR-book/html/htmledition/tf-idf-weighting-1.html).

Then, I took it a step further and built a recommender system driven by the
public data on [MyAnimeList](https://myanimelist.net/). By providing the username
of an account on the website, I collect the publicly available watch list of
that account and perform a custom-written query on a computed graph of
recommendations over their corpus of shows. 

This work was heavily inspired by previous search engine I developed for the
corpus of [arXiv preprint metadata](https://arxiv.org/) which was powered by
[non-negative matrix
factorization](https://en.wikipedia.org/wiki/Non-negative_matrix_factorization).
[More on that in this blog
post](https://derekrodriguez.dev/a-recommender-system-for-arxiv-in-500-lines-of-python-and-other-lies-i-tell-myself/)

![](/assets/koi_no_yokan.jpg)