---
title: "Crypto and American business run in opposite directions"
date: 2022-11-10
draft: true
---

I need to finish my thesis, but I can't get the headlines surrounding FTX out of
my head so hopefully by writing this I can clear my mind. The goal of this essay
is to help clarify why the space surrounding cryptocurrency seems so rife with
problems. For disclosure, I find the concept of decentralization fascinating,
and while I've been watching the cryptocurrency space develop from the sidelines
for quite some time, take my opinions with as much of a grain of salt as your
    average armchair economist.


