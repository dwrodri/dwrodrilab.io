---
title: "Advent of Code 2017 Day 5"
date: 2023-06-13T18:15:45+00:00
draft: true
---

```python
from typing import List


def load_input(filename: str) -> List[int]:
    with open(filename) as fp:
        return [int(line) for line in fp]


def simulate_jumps(table: List[int], is_part_one=True) -> int:
    pc = 0
    counter = 0
    while 0 <= pc < len(table):
        offset = table[pc]
        if not is_part_one and table[pc] >= 3:
            table[pc] -= 1
        else:
            table[pc] += 1
        pc += offset
        counter += 1
    return counter


if __name__ == "__main__":
    print(simulate_jumps(load_input("inputs/day5.txt"), is_part_one=False))
```
