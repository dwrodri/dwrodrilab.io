---
title: "Notes on Interrupts and Exceptions in RISC-V"
date: 2020-09-14
draft: true
---

# Overview 

- what and why of interrupts/exceptions

- Step-by-step walkthrough of the spec for an I/O interrupt

- Worked example of pipeline with syscall 

- Summary of the CLIC and the PLIC

- Security implications of speculative exec done in server-class microarchitectures and thoughts on how we can "harden" CLIC/PLIC against these types of attacks. 

- Do we even need these features? Should do mirobenchmark comparison of Ryzen vs. Boom to see what the differential is currently.
