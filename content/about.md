I'm a current member of the NUCAR Lab at Northeastern University, studying under
[Prof. David Kaeli](https://ece.northeastern.edu/fac-ece/kaeli.html). Before
that, I got my B.Sc. in Computer Science at Clemson University and worked under
[Prof. Rong Ge](https://people.computing.clemson.edu/~rge/) looking at
performance engineering challenges in GPU clusters.

When I'm not programming, working on school, or writing stuff to post here, I
like to record and produce music. I started off with electronic music, 
but have since expanded into jazz and rock/metal. When I'm not making
music, I'm gaming, playing squash, skateboarding, or looking for a warm tropical
beach to sit on.
