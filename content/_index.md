I'm an ML Engineer with experience in both computer vision and NLP wrapping up a
graduate degree. I studied at Northeastern, specifically at the [NUCAR
lab](https://ece.northeastern.edu/groups/nucar/people.html).

I like writing about:
- Python
- Applied statistics and machine learning
- Search engines and information retrieval
- Low-level programming stuff, especially performance engineering

I love getting email from real people who have real feedback about what I say 
and what I make. In an effort to minimize plugging my own stuff on social media, 
go ahead and send me an email if you didn't find this blog via Reddit/HN/Lobsters/Slashdot.
